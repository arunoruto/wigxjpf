(See https://github.com/pypa/setuptools/issues/391#issuecomment-202919511 )

Inline usage information
------------------------

Available inline::

    import pywigxjpf as wig
    help(wig)                # For interfaces.
    help(wig.pywigxjpf)      # For usage information.

Library usage
-------------

The python interface to wigxjpf uses cffi.

Defines seven functions::

    wig_table_init(max_two_j,wigner_type)
    wig_table_free()
    wig_temp_init(max_two_j)
    wig_temp_free()

    wig3jj(jj1,jj2,jj3, mm1,mm2,mm3)
    wig6jj(jj1,jj2,jj3, jj4,jj5,jj6)
    wig9jj(jj1,jj2,jj3, jj4,jj5,jj6, jj7,jj8,jj9)

Note that the arguments are to be given as integers, with
twice the numeric value (this is what jj tries to indicate).
I.e. half-integer arguments will be passed as odd integers.

The two init functions must be called before evaluating any
symbol.

In addition, interfaces that take an array with the arguments
are also provided::

    wig3jj_array([jj1,jj2,jj3, mm1,mm2,mm3])
    wig6jj_array([jj1,jj2,jj3, jj4,jj5,jj6])
    wig9jj_array([jj1,jj2,jj3, jj4,jj5,jj6, jj7,jj8,jj9])